//Classes
function Jogada(tipo, perdeDe, ganhaDe){
    this.Tipo = tipo;
    this.Perde = perdeDe;
    this.Ganha = ganhaDe;
}

function Jogador(nome, sexo, tipoJogador){
    this.Nome = nome;
    this.Sexo = sexo;
    this.TipoJogador = tipoJogador;
    this.Jogada = '';
    this.Pontos = 0;
    this.DuracaoPartida;
    this.TotalPartidas = 0;


    this.OpcaoSelecionada = function(){
        if(this.TipoJogador == "bot"){
            var jogadaAleatoria = Math.floor(Math.random() * 3);
            if(jogadaAleatoria == 0){
                this.Jogada = new Jogada("Pedra","Papel", "Tesoura");
            }
            else if(jogadaAleatoria == 1){
                this.Jogada = new Jogada("Papel", "Tesoura", "Pedra");
            }
            else{
                this.Jogada = new Jogada("Tesoura","Pedra", "Papel");
            }
            return this.Jogada;
        }else{
            return "";
        }
    }
}

var CRONOMETRO = 0;
function main(){
    var participantes = selecionarJogadores();
    var jogador = participantes[0];
    var bot = participantes[1];

    if(CRONOMETRO == 0){
        CRONOMETRO = iniciarPartida(jogador, bot);
    }    
    
    $("#pedra").click(function(){
        desabilitarInterface();
        var pedra= new Jogada("Pedra", "Papel", "Tesoura");
        jogador.Jogada = pedra;
        clearInterval(CRONOMETRO);
        duracao = $("#horas").text()+":"+$("#minutos").text()+":"+$("#segundos").text();
        jogador.DuracaoPartida = duracao;
        jogador.TotalPartidas += 1;

        bot.OpcaoSelecionada();
        bot.DuracaoPartida = duracao;       
        bot.TotalPartidas += 1;

        verQuemGanhou(jogador, bot);
    });

    $("#papel").click(function(){
        desabilitarInterface();
        var papel = new Jogada("Papel", "Tesoura", "Pedra");
        jogador.Jogada = papel;
        clearInterval(CRONOMETRO);
        duracao = $("#horas").text()+":"+$("#minutos").text()+":"+$("#segundos").text();
        jogador.DuracaoPartida = duracao;
        jogador.TotalPartidas += 1;
        
        bot.OpcaoSelecionada();
        bot.DuracaoPartida = duracao;
        bot.TotalPartidas += 1;

        verQuemGanhou(jogador, bot);

        
    });

    $("#tesoura").click(function(){
        desabilitarInterface();
        var tesoura = new Jogada("Tesoura", "Pedra", "Papel");
        jogador.Jogada = tesoura;
        clearInterval(CRONOMETRO);
        duracao = $("#horas").text()+":"+$("#minutos").text()+":"+$("#segundos").text();
        jogador.DuracaoPartida = duracao;
        jogador.TotalPartidas += 1;
        
        bot.OpcaoSelecionada();
        bot.DuracaoPartida = duracao;
        bot.TotalPartidas += 1;
        
        verQuemGanhou(jogador, bot);
    });


    $("#btn-continuar").click(function(){
        CRONOMETRO = 0;
        $("#horas").text("00");
        $("#minutos").text("00");
        $("#segundos").text("00");

        $("#pedra").attr("disabled", false);
        $("#papel").attr("disabled", false);
        $("#tesoura").attr("disabled", false);
        $("#alerta").text("");

        CRONOMETRO = iniciarPartida(jogador, bot);
    });

    console.log("\nvoltei pra main");


}

function desabilitarInterface(){
    $("#pedra").attr("disabled", true);
    $("#papel").attr("disabled", true);
    $("#tesoura").attr("disabled", true);
}

function selecionarJogadores(){
    var jogadores = [];
    //var jogador = montarJogador();
    var jogador = new Jogador("Jessica", "feminino", "humano");
    var bot = new Jogador("BotSinistrao", "naoDefinido", "bot");
    jogadores[0] = jogador;
    jogadores[1] = bot;
    return jogadores;
}

function montarJogador(){
    var condicao = true;
    var opcao, nome, sexo;
    do{
        opcao = prompt("Como você deseja jogar?\nComo humano, insira: 'humano'\nComo bot, insira: 'bot'");
        if(opcao == "humano"){
            nome = prompt("Insira o seu nome:");
            sexo = prompt("Insira se vc pertence ao sexo masculino ou feminino:");
            condicao = false;
        }
        else if(opcao == "bot"){
            nome = prompt("Insira o nome do seu robô:");
            sexo = prompt("Insira se o seu robô é macho ou fêmea:");
            condicao = false;
        }
        else{
            alert("Você precisa ser um humano ou um robô para jogar");
            
        }
    }
    while(condicao)

    jogador = new Jogador(nome, sexo, opcao);

    return jogador;
}

function iniciarPartida(j1, j2){
    if (j1.Sexo == "masculino"){
        $("#boas-vindas").text("Olar senhor " + j1.Nome);
    }
    else{
        $("#boas-vindas").text("Olar senhora " + j1.Nome);
    }

    return iniciaCronometro();   
}

function iniciaCronometro(){
    var segundos = $("#segundos").text();
    var minutos = $("#minutos").text();
    var horas = $("#horas").text();
    var crono = setInterval(function(){       
        if(horas <= 24){
            if(minutos <= 59){
                if(segundos <= 59){
                    segundos ++;                    
                    $("#segundos").text(segundos);
                    
                }
                else{
                    segundos = 0
                    $("#segundos").text(segundos);
                    minutos ++;
                    $("#minutos").text(minutos);
                }
            }
            else{
                segundos = 0
                minutos = 0
                $("#segundos").text(segundos);
                $("#minutos").text(minutos);
                horas ++;
                $("#horas").text(horas);
            }            
        }
        else{
            segundos = 0
            minutos = 0
            $("#segundos").text(segundos);
            $("#minutos").text(minutos);
            horas ++;
            $("#horas").text(horas);
        }        
        return;
    }, 1000);

    return crono;
}

function verQuemGanhou(j1, j2){
    if(j1.Jogada.Tipo == j2.Jogada.Perde){
        console.log(j1.Nome+" ganhou com "+j1.Jogada.Tipo+" contra "+j2.Jogada.Tipo);
        //console.log("duração:"+j1.DuracaoPartida);
        
        j1.Pontos += 3;
        atualizaPlacar(j1, j2);
    }
    else if(j2.Jogada.Tipo == j1.Jogada.Perde){
        console.log(j2.Nome+" ganhou com "+j2.Jogada.Tipo+" contra "+j1.Jogada.Tipo);
        //console.log("duração:"+j2.DuracaoPartida);
        
        j2.Pontos += 3;
        atualizaPlacar(j1, j2);
    }
    else if(j1.Jogada.Tipo == j2.Jogada.Tipo){
        console.log(j1.Jogada.Tipo + " empata com "+ j2.Jogada.Tipo);
        //console.log("duração:"+j1.DuracaoPartida);
        //console.log("duração:"+j2.DuracaoPartida);

        
        j1.Pontos += 1;
        j2.Pontos += 1;
        atualizaPlacar(j1, j2);
    }
    return;
}

function atualizaPlacar(j1, j2){    
    var corpoTabela = $(".placar").find("tbody");    
    var tds = document.querySelectorAll("tbody td");    
    if(tds.length == 0){
        //primeira execussão, tabela vazia
        var linha = "<tr>"+
        "<td>"+j1.Nome+"</td>"+
        "<td>"+j1.Jogada.Tipo+"</td>"+
        "<td>"+j1.DuracaoPartida+"</td>"+
        "<td>"+j1.TotalPartidas+"</td>"+
        "<td>"+j1.Pontos+"</td>"
        +"</tr>";    
        corpoTabela.append(linha);

        var linha = "<tr>"+
        "<td>"+j2.Nome+"</td>"+
        "<td>"+j2.Jogada.Tipo+"</td>"+
        "<td>"+j2.DuracaoPartida+"</td>"+
        "<td>"+j2.TotalPartidas+"</td>"+
        "<td>"+j2.Pontos+"</td>"
        +"</tr>";
        corpoTabela.append(linha);
    }
    else{
        atualizaPlacarJogador(j1);
        atualizaPlacarJogador(j2);
    }
}
function atualizaPlacarJogador(jogador){
    console.log("chamei atualiza placar jogador para "+jogador.Nome);
    
    var corpoTabela = $(".placar").find("tbody");
    //console.log(corpoTabela);
    var tds = document.querySelectorAll("tbody td");
    //console.log(tds);    
    var i;
    for(i = 0; i < tds.length; i+=5){
        console.log("Entrei no for");
        var nome = tds[i].innerHTML;
        if(nome == jogador.Nome){            
            console.log("vamos atualizar "+nome);

            var jogada = tds[(i+1)].innerHTML;
            tds[(i+1)].innerHTML = jogador.Jogada.Tipo;
            console.log("jogada antiga "+jogada);

            var tempoJ = tds[(i+2)].innerHTML;
            console.log("tempo antiga "+tempoJ);

            var totalAnt = tds[(i+3)].innerHTML;
            tds[(i+3)].innerHTML = jogador.TotalPartidas;
            console.log("total antiga "+totalAnt);

            var result = tds[(i+4)].innerHTML;
            tds[(i+4)].innerHTML = jogador.Pontos;
            console.log("resultado antiga "+result);

        }
        else{
            //aqui vai ser tratado quando o jogador for trocado
        }
        console.log("###############################\n\n")
    }
}
