function Jogada(tipo, perdeDe, ganhaDe){
    this.Tipo = tipo;
    this.Perde = perdeDe;
    this.Ganha = ganhaDe;
}

function Jogador(nome, sexo, tipoJogador){
    this.Nome = nome;
    this.Sexo = sexo;
    this.TipoJogador = tipoJogador;
    this.Jogada = "";
    this.Pontos = 0;
    this.DuracaoPartida;
    this.TotalPartidas = 0;


    this.OpcaoSelecionada = function(){
        if(this.TipoJogador == "bot"){
            var jogadaAleatoria = Math.floor(Math.random() * 3);
            if(jogadaAleatoria == 0){
                this.Jogada = new Jogada("Pedra","Papel", "Tesoura");
            }
            else if(jogadaAleatoria == 1){
                this.Jogada = new Jogada("Papel", "Tesoura", "Pedra");
            }
            else{
                this.Jogada = new Jogada("Tesoura","Pedra", "Papel");
            }
            return this.Jogada;
        }else{
            return "";
        }
    }
}

CONTADOR = 0;
CONTADORVERQUEMGANHA = 0;
function selecionarJogadores(){
    var jogadores = [];
    //var jogador = montarJogador();
    var jogador = new Jogador("Jessica", "feminino", "humano");
    var bot = new Jogador("BotSinistrao", "naoDefinido", "bot");
    jogadores[0] = jogador;
    jogadores[1] = bot;
    return jogadores;
}

function montarJogador(){
    var condicao = true;
    var opcao, nome, sexo;
    do{
        opcao = prompt("Como você deseja jogar?\nComo humano, insira: 'humano'\nComo bot, insira: 'bot'");
        if(opcao == "humano"){
            nome = prompt("Insira o seu nome:");
            sexo = prompt("Insira se vc pertence ao sexo masculino ou feminino:");
            condicao = false;
        }
        else if(opcao == "bot"){
            nome = prompt("Insira o nome do seu robô:");
            sexo = prompt("Insira se o seu robô é macho ou fêmea:");
            condicao = false;
        }
        else{
            alert("Você precisa ser um humano ou um robô para jogar");
            
        }
    }
    while(condicao)

    jogador = new Jogador(nome, sexo, opcao);

    return jogador;
}

function iniciaCronometro(){
    var segundos = $("#segundos").text();
    var minutos = $("#minutos").text();
    var horas = $("#horas").text();
    var crono = setInterval(function(){       
        if(horas <= 24){
            if(minutos <= 59){
                if(segundos <= 59){
                    segundos ++;                    
                    $("#segundos").text(segundos);
                    
                }
                else{
                    segundos = 0
                    $("#segundos").text(segundos);
                    minutos ++;
                    $("#minutos").text(minutos);
                }
            }
            else{
                segundos = 0
                minutos = 0
                $("#segundos").text(segundos);
                $("#minutos").text(minutos);
                horas ++;
                $("#horas").text(horas);
            }            
        }
        else{
            segundos = 0
            minutos = 0
            $("#segundos").text(segundos);
            $("#minutos").text(minutos);
            horas ++;
            $("#horas").text(horas);
        }        
        return;
    }, 1000);

    return crono;
}

function atualizaPlacar(jogador){
    console.log("chamei atualiza placar para "+jogador.Nome);
    var corpoTabela = $(".placar").find("tbody");
    console.log(corpoTabela);
    var tds = document.querySelectorAll("tbody td");
    var i;
    for(i = 0; i < tds.length; i+=5){
        var nome = tds[i].innerHTML;
        if(nome == jogador.Nome){
            console.log("vamos atualizar");
            break;            
        }
        else{
            var linha = "<tr>"+
                            "<td>"+jogador.Nome+"</td>"+
                            "<td>"+jogador.Jogada.Tipo+"</td>"+
                            "<td>"+jogador.DuracaoPartida+"</td>"+
                            "<td>"+jogador.TotalPartidas+"</td>"+
                            "<td>"+jogador.Pontos+"</td>"
                        +"</tr>";
            console.log(linha);
            corpoTabela.append(linha);
            break;
        }
    }
}

function verQuemGanha(jogador1, jogador2){
    CONTADORVERQUEMGANHA++
    console.log(CONTADORVERQUEMGANHA);
    var jogadaBot = jogador2.OpcaoSelecionada();
    //console.log(" Jogador 1: "+jogador1.Jogada.Tipo + "\nJogador 2 : "+jogador2.OpcaoSelecionada().Tipo);
    if(jogador1.Jogada.Tipo == jogadaBot.Perde){
        var j1 = "j1 Tipo: "+jogador1.Jogada.Tipo +" Ganha:"+jogador1.Jogada.Ganha+" Perde: "+jogador1.Jogada.Perde;
        var j2 = jogadaBot;

        console.log(j1);
        console.log("j2 Tipo: "+j2.Tipo+" Ganha: "+j2.Ganha+" Perde: "+j2.Perde);
        console.log("humano ganhou do bot");
        //jogador 1 ganhou        
        //jogador1.Pontos += 3;
        //console.log(jogador1.Nome + "ganhou "+jogador1.Pontos);
        //atualizaPlacar(jogador1);
        return;
        
    }
    else if (jogadaBot.Tipo == jogador1.Jogada.Perde){
        var j1 = "j1 Tipo: "+jogador1.Jogada.Tipo +" Ganha:"+jogador1.Jogada.Ganha+" Perde: "+jogador1.Jogada.Perde;
        var j2 = jogadaBot;

        console.log(j1);
        console.log("j2 Tipo: "+j2.Tipo+" Ganha: "+j2.Ganha+" Perde: "+j2.Perde);
        console.log("bot ganhou de humano");
        //jogador 2 ganhou        
        //jogador2.Pontos += 3;
        //console.log(jogador2.Nome + "ganhou "+jogador2.Pontos);
        //atualizaPlacar(jogador2);
        return;
    }
    else if(jogador1.Jogada.Tipo == jogador2.OpcaoSelecionada().Tipo){
        //empate
        //jogador1.Pontos += 1;
        //jogador2.Pontos += 1;
        //console.log("Enpate");
        return;
    }
}

function novaPartida(jogador1, jogador2){
    CONTADOR++
    console.log(CONTADOR);
    if (jogador1.Sexo == "masculino"){
        //console.log("jogador masculino");
        $("#boas-vindas").text("Olar senhor " + jogador1.Nome);
    }
    else{
        $("#boas-vindas").text("Olar senhora " + jogador1.Nome);
    }
    var chronometroId = iniciaCronometro();

    $("#pedra").click(function(){        
        clearInterval(chronometroId);
        var pedra= new Jogada("Pedra", "Papel", "Tesoura");
        jogador1.Jogada = pedra;
        verQuemGanha(jogador1, bot);

    });
    
    $("#papel").click(function(){
        clearInterval(chronometroId);
        var papel = new Jogada("Papel", "Tesoura", "Pedra");
        jogador1.Jogada = papel;
        verQuemGanha(jogador1, bot);
    });

    $("#tesoura").click(function(){
        clearInterval(chronometroId);
        var tesoura = new Jogada("Tesoura", "Pedra", "Papel");
        jogador1.Jogada = tesoura;
        verQuemGanha(jogador1, bot);
    });
};
function main(){
    var participantes = selecionarJogadores();
    jogador = participantes[0];
    bot = participantes[1];

    novaPartida(jogador, bot);

    $("#btn-continuar").click(function(){

        $("#horas").text("00");
        $("#minutos").text("00");
        $("#segundos").text("00");

        $("#pedra").attr("disabled", false);
        $("#papel").attr("disabled", false);
        $("#tesoura").attr("disabled", false);
        $("#alerta").text("");
        novaPartida(jogador, bot);
    });   
}